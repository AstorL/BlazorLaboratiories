﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Blazor_laboratories.Data
{
    public class Four_digits
    {
        Random rnd = new Random();
        char[] enigma = new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

        public Four_digits()
        {
            NewGame();
        }

        public void NewGame()
        {
            for (int i = 0; i < 4; i++)
            {
                var j = rnd.Next(10);
                var t = enigma[i]; enigma[i] = enigma[j]; enigma[j] = t;
            }
            Steps.Clear();
            Input = "";
        }

        string input;
        [Required]
        public string Input
        {
            get { return input; }
            set
            {
                input = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Input"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Error"));
                }
            }
        }

        public string Error
        {
            get
            {
                if (input.Length != 4 || !input.All(ch => char.IsDigit(ch)))
                    return "Нужно строго четыре цифры!!!";
                else if (input.GroupBy(x => x).Count() != 4)
                    return "Все цифры должны быть разные!!!";
                else
                    return "";
            }
        }

        public ObservableCollection<string> Steps { get; }
          = new ObservableCollection<string>();

        public void Step()
        {
            if (Error == "")
            {
                var nums = input;
                int b = 0, c = 0;
                for (int i = 0; i < 4; i++)
                    if (nums[i] == enigma[i]) b++;
                    else for (int j = 0; j < 4; j++)
                            if (nums[i] == enigma[j]) c++;
                Steps.Insert(0, nums + "  " + b + "/" + c);
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
